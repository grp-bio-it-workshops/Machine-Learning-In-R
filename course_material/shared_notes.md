## Bio-IT Course: Machine Learning in R (advanced course)

### Dates: 12/06/2018 - 13/06/2018
### Time: 09:30 - 17:30

### Tutors and helpers

– Prof Bernd Bischl, Xudong Sun (Ludwig-Maximilians-University Munich)
– Michel Lang (TU Dortmund)
– Georg Zeller, Bernd Klaus (EMBL Heidelberg)

#### Bio-IT course page: https://bio-it.embl.de/events/machine-learning-in-r-advanced-course/
#### de.NBI course page: https://www.hd-hub.de/course-dates/3-all/31-machine-learning-in-r-advanced-course
#### Git repo: https://git.embl.de/grp-bio-it/Machine-Learning-In-R

---------------------------------

## Session -1 & Session -2

https://tinyurl.com/mlembl2018

### Bernd Bischl and Michel Minel: https://oc.embl.de/index.php/s/VGhFsR5X94k5cjG#pdfviewer

### Bernd Klaus: 

- Slides:https://oc.embl.de/index.php/s/VGhFsR5X94k5cjG/download?path=%2Fknn_based_intro&files=knn_ml_intro.html
        - Other data: https://oc.embl.de/index.php/s/VGhFsR5X94k5cjG?path=%2Fknn_based_intro
        - RMD file: https://oc.embl.de/index.php/s/VGhFsR5X94k5cjG/download?path=%2Fknn_based_intro&files=knn_ml_intro.Rmd
        - Download RMD file -> open in R studio -> Knit -> create the MD file
                - R code: https://oc.embl.de/index.php/s/VGhFsR5X94k5cjG/download?path=%2Fknn_based_intro&files=knn_ml_intro.R

### Georg Zeller: https://github.com/gezel/mlr_workshop_2018

### Other links:

- MLR tutorial: https://github.com/mlr-org/mlr
- https://kaggle.com/c/titanic:  Titanic dataset is a starting dataset in Kaggle and doesn't require a lot pre-processing
- Rda file can be directly copied from the slides here: https://oc.embl.de/index.php/s/VGhFsR5X94k5cjG#pdfviewer 
- Bio-IT Regular Expressions Tutorial: https://legacy.gitbook.com/book/tobyhodges/introduction-to-regular-expressions/details (there are lots of other good tutorials online too :) )

### Click the link to participate in the competition

- kaggle link: https://www.kaggle.com/t/96c5be594b9841b68843cccce80a3833


In the github 
normalised "training" data:
train_data_FR.tsv

normalised "test" data:
test_data_CN.tsv

On kaggle
normalized data is called
train_data_FR_norm.csv
test_data_FR_norm.csv

------

## Additional material on how (not) to combine feature selection and cross validation:

- "Pitfalls of supervised feature selection (DOI: 10.1093/bioinformatics/btp621)
- Chapter 7.10 in "The Elements of Statistical Learning" (https://web.stanford.edu/~hastie/Papers/ESLII.pdf)
