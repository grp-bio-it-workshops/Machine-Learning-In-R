## Prerequisites

- The course is aimed at participants preferably with some knowledge of statistics and data modeling, and want to learn more about machine learning and its application and implementation through the hands-on sessions and use cases. 
- The participants are expected to understand the concepts described in [these materials](http://www-huber.embl.de/users/klaus/tidyverse_R_intro/R-lab.html#7_simple_plotting_in_r:_qplot_of_ggplot2) before the workshop.
- Participants are expected to bring their own laptop with R version >=3.3.2 installed.
- Please create a [Kaggle account](https://www.kaggle.com/) for the hands-on sessions.
- Optional: The participants can have a look at the [mlr tutorial](https://mlr-org.github.io/mlr/) to gain a little head-start, but this will be covered in the lectures.