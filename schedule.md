# Course content

## This 2-day course will cover the following topics and sessions:

### Sessions on Day-1

1. Introduction to Machine Learning, mlr, KNN, and its _application in a biological dataset_
2. Linear models, regression, regularization, and trees
3. Evaluation (train, test, ROC), and its _application to microbiome-based cancer detection_
4. Hands-on session: application on a new dataset (see the prerequisite #3)

### Sessions on Day-2

5. Forests and boosting with a demo
6. Tuning and nested resampling with a demo
7. Interpretable machine learning and feature selection
8. Hands-on session: application on the dataset from Session-4