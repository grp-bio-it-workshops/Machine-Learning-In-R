# Machine Learning in R (advanced course)

## About the course

### Date/Time

- Date(s) - 12/06/2018 - 13/06/2018
- 09:30 - 17:30

### Tutors and helpers

– Prof Bernd Bischl, Xudong Sun (Ludwig-Maximilians-University Munich)
– Michel Lang (TU Dortmund)
– Georg Zeller, Bernd Klaus (EMBL Heidelberg)

Organised by the Bio-IT Project

### Course Information

This two-day course, delivered by experts in programming for data analysis, will teach participants the principle of machine learning and its implementation in R using mlr package. The main goal of mlr is to provide a unified interface for machine learning tasks as classification, regression, cluster analysis and survival analysis in R.

Sessions will be driven by many practical exercises and case studies.

Prior to the course, on June 11, 2018, 2:00 PM at Small Operon, an open seminar will be delivered by Prof. Bernd Bischl. **Title TBA**. He will also be leading the informal event 'Technical session with Bio-IT and de.NBier' on the 'OpenML' project at EMBL staff lounge followed by a beer session.


